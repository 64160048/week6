package com.nimmak.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       BookBank thitiphong = new BookBank("thitiphong",50.0); // Constructer
       
       thitiphong.print();
       BookBank prayud = new BookBank("prayud",100000.0);
       
       prayud.print();
       prayud.withdraw(40000.0);
       prayud.print();

       thitiphong.deposit(40000.0);
       thitiphong.print();
       
       BookBank prawit = new BookBank("prawit",1000000.0);
       
       prawit.print();
       prawit.deposit(2.0);
       prawit.print();

    }
}
