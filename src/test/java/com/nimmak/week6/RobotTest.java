package com.nimmak.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());

    }

    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());

    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());

    }

    @Test
    public void shouldupNegative() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());

    }

    @Test
    public void shouldDownSuccess1() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals(true, robot.down(12));
        assertEquals(12, robot.getY());

    }

    @Test
    public void shouldLeftSuccess1() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals(true, robot.left(4));
        assertEquals(0, robot.getY());

    }

    @Test
    public void shouldLeftSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals(true, robot.left(6));
        assertEquals(0, robot.getY());

    }
    
    
    @Test
    public void shouldUptSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals(false, robot.up(6));
        assertEquals(0, robot.getY());

    }
  
    @Test
    public void shouldLeftFail1() {
        Robot robot = new Robot("Robot", 'R',0,0);
        assertEquals(false , robot.left(52));
        assertEquals(0, robot.getY());

    }
    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());

    }

    @Test
    public void shouldUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());

    }

    @Test
    public void shouldUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());

    }

    @Test
    public void shouldUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());

    }

    @Test
    public void shouldUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());

    }

    @Test
    public void shouldleftSucces() {
        Robot ro = new Robot("Robot", 'H', 1, 0);
        assertEquals(true, ro.left());
        assertEquals(0, ro.getY());
    }

    @Test
    public void shouldrighttSucces1() {
        Robot ro = new Robot("Robot", 'H', 0, 1);
        assertEquals(true, ro.right());
        assertEquals(1, ro.getY());
    }
    
    @Test
    public void shouldRightSucces2() {
        Robot ro = new Robot("Robot", 'H');
        assertEquals(true, ro.right(6));
        assertEquals(6, ro.getX());
    }

   
    @Test
    public void shouldRightFail1() {
        Robot ro = new Robot("Robot", 'H');
        assertEquals(false, ro.right(29));
        assertEquals(19, ro.getX()); 
    }
}
