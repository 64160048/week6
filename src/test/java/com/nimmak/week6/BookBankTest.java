package com.nimmak.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess() {
        BookBank book = new BookBank("thitiphong" , 100);
        book.withdraw(50);
        assertEquals(50, book.getBalance(),0.00001);
    }
    
    @Test
    public void shouldWithdrawOverbalance() {
        BookBank book = new BookBank("thitiphong" , 100);
        book.withdraw(150);
        assertEquals(100, book.getBalance(),0.00001);
    }
    
    
    @Test
    public void shouldWithdrawWithNegativeNumber() {
        BookBank book = new BookBank("thitiphong" , 100);
        book.withdraw(-100);
        assertEquals(100, book.getBalance(),0.00001);
    }
}
